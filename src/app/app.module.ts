import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import {FeatureModule} from './features/feature.module';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
// tslint:disable-next-line:import-spacing
import { TranslateModule, TranslateLoader, TranslateService } from  '@ngx-translate/core';
// tslint:disable-next-line:import-spacing
import { TranslateHttpLoader } from  '@ngx-translate/http-loader';
import {HttpClientModule, HttpClient} from '@angular/common/http';


export  function  HttpLoaderFactory(http: HttpClient) {
  return  new  TranslateHttpLoader(http, './assets/translate', '.json');
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MDBBootstrapModule.forRoot(),
    FeatureModule,
    CoreModule,
    SharedModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide:  TranslateLoader,
        useFactory:  HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  exports: [TranslateModule],
  providers: [
    TranslateService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
