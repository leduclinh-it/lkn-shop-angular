export const DATE_NOW = 'HH:mm:ss (MM/dd/yyyy)';
export const DATE = 'MM/dd/yyyy';
export const FrontURL = {
  HOME: 'home',
  PRODUCT_LIST: 'product-list',
  PRODUCT_DETAIL: 'product-detail',
  LOGIN: 'login',
  REGISTER: 'register',
  ADMIN: 'admin',
  CART: 'cart',
  CHECK_OUT: 'check-out',
  ACCOUNT: 'account',
  DASHBOARD: 'dashboard',
  USER_LIST: 'user-list',
  ORDER_NEW: 'order-new',
  ORDER_COMPLETE: 'order-complete',
  ORDER_CANCEL: 'order-cancel',
  PRODUCT_MANAGE: 'product-manage',
  ORDER_DETAIL: 'order-detail/:status/:id',
  NEWS: 'news',
  ABOUT: 'about',
  ORDER_USER: 'order-user/:status',
  EMAIL_CONFIRM: 'email-confirm',
  CONFIRM_PASSWORD: 'confirm'
};
export const Local = {
  CURRENT_USER: 'currentUser',
  CART: 'cart',
  TOKEN: 'token'
};
Object.freeze(DATE_NOW);
Object.freeze(DATE);
Object.freeze(FrontURL);
Object.freeze(Local);

