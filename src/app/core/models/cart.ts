import {OrderDetail} from './orderDetail';

export class Cart {
  orderDetail: Array<OrderDetail>;
  total: number ;

  constructor(orderDetail: Array<OrderDetail>, total: number = 0) {
    this.orderDetail = orderDetail;
    this.total = total;
  }
}
