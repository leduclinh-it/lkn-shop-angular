export class Comment {
  dateComment: Date;
  idUser: number;
  userImage: string;
  userName: string;
  content: string;
  constructor(dateComment: Date, idUser: number, userImage: string, userName: string, content: string) {
    this.dateComment = dateComment;
    this.idUser = idUser;
    this.userImage = userImage;
    this.userName = userName;
    this.content = content;
  }
}
