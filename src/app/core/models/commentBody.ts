import {Comment} from './comment';

export class CommentBody {
  idProduct: number;
  commentProduct: Comment;


  constructor(idProduct: number, commentProduct: Comment) {
    this.idProduct = idProduct;
    this.commentProduct = commentProduct;
  }
}
