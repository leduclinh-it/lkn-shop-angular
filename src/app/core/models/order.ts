import {User} from './user';
import {OrderDetail} from './orderDetail';

export class Order {
  public id: number;
  public date: Date;
  public discount: number;
  public total: number;
  public custommer: User;
  public oderStatus: number;
  public details: Array<OrderDetail>;


  constructor(id: number = 0 ,
              date: Date = new Date(),
              discount: number = 0,
              total: number = 0,
              customer: User = new User(),
              oderStatus: number = 0,
              orderDetail: Array<OrderDetail> = new Array<OrderDetail>()) {
    this.id = id;
    this.date = date;
    this.discount = discount;
    this.total = total;
    this.custommer = customer;
    this.oderStatus = oderStatus;
    this.details =  orderDetail;
  }
}
