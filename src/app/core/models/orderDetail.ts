import {Product} from './product';

export class OrderDetail {
  public product: Product;
  public quanity: number;
  public price: number;
  public total: number;


  constructor(product: Product = new Product(),
              quantity: number = 0,
              price: number = 0,
              total: number = 0) {
    this.product = product;
    this.quanity = quantity;
    this.price = price;
    this.total = total;
  }
}
