import {Comment} from './comment';
import * as _ from 'lodash';
export class Product {
  id: number;
  name: string;
  image: Array<string>;
  price: number;
  color: Array<number>;
  brand: string;
  size: Array<number>;
  type: string;
  object: string;
  status: string;
  description: string;
  commentProducts: Array<Comment>;
  static getInstance(obj?: any): Product {
    if (obj) {
      const objParse = _.isObject(obj) ? obj : JSON.parse(obj);
      const image = objParse.image;
      const color = objParse.color;
      const size = objParse.size;


      return new Product(objParse.id,
        objParse.name,
        image,
        objParse.price,
        color,
        objParse.brand,
        size,
        objParse.type,
        objParse.object,
        objParse.status,
        objParse.description);
    } else {
      return new Product();
    }
  }

  constructor(id: number = 0,
              name: string = '',
              image: Array<string> = new Array<string>(),
              price: number = 0,
              color: Array<number> = new Array<number>(),
              brand: string = '',
              size: Array<number> = new Array<number>(),
              type: string = '',
              object: string = '',
              status: string = '',
              description: string = '',
              commentProducts: Array<Comment> = new Array<Comment>()) {
    this.id = id;
    this.name = name;
    this.image = image;
    this.price = price;
    this.color = color;
    this.brand = brand;
    this.size = size;
    this.type = type;
    this.object = object;
    this.status = status;
    this.description = description;
    this.commentProducts = commentProducts;
  }
}
