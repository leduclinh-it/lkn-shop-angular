export class Profile {
  firstName: string;
  lastName: string;
  phoneNumber: string;
  address: string;

  constructor(firstName: string = '',
              lastName: string = '',
              phoneNumber: string = '',
              address: string = '') {
    this.firstName = firstName;
    this.lastName = lastName;
    this.phoneNumber = phoneNumber;
    this.address = address;
  }
}
