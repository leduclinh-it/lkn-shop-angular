import {Role} from './role';

export class User {
  id: number;
  username: string;
  image: string;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  email: string;
  earnPoints: number;
  address: string;
  token: string;
  roles: Array<Role>;


  constructor(id: number = 0,
              username: string = '',
              image: string = '',
              firstName: string = '',
              lastName: string = '',
              phoneNumber: string = '',
              email: string = '',
              earnPoints: number = 0,
              address: string = '',
              token: string = '',
              roles: Array<Role> = new Array<Role>()) {
    this.id = id;
    this.username = username;
    this.image = image;
    this.firstName = firstName;
    this.lastName = lastName;
    this.phoneNumber = phoneNumber;
    this.email = email;
    this.earnPoints = earnPoints;
    this.address = address;
    this.token = token;
    this.roles = roles;
  }
}
