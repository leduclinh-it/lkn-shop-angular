export class UserRegister {
  image: string;
  email: string;
  username: string;
  password: string;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  earnPoints: number;
  address: string;
  enabled: boolean;
  constructor(
    image: string = '',
    email: string = '',
    username: string = '',
    password: string = '',
    firstName: string = '',
    lastName: string = '',
    phoneNumber: string = '',
    earnPoints: number = 0,
    address: string = '',
    enabled: boolean = false) {
    this.image = image;
    this.email = email;
    this.username = username;
    this.password = password;
    this.firstName = firstName;
    this.lastName = lastName;
    this.phoneNumber = phoneNumber;
    this.earnPoints = earnPoints;
    this.address = address;
    this.enabled = enabled;
  }
}
