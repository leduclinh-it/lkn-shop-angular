import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {User} from '../models/user';
import {environment} from '../../../environments/environment';
import {NewPassword} from '../models/newPassword';
import {Profile} from '../models/profile';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {UserRegister} from '../models/userRegister';
import * as CONST from '../constants';
import {ResetPassword} from '../models/resetPassword';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  private readonly API_URL_EDIT_PROFILE = environment.apiUrl.user.editProfile;
  private readonly API_URL_UPDATE_PASSWORD = environment.apiUrl.user.updatePassword;
  private readonly API_URL_REGISTER = environment.apiUrl.account.register;
  private readonly API_URL_FORGOT_PASSWORD = environment.apiUrl.user.forgotPassword;
  private readonly API_URL_RESET_PASSWORD = environment.apiUrl.user.resetPassword;
  public updateProfile = new BehaviorSubject(JSON.parse(sessionStorage.getItem(CONST.Local.CURRENT_USER)));
  public currentUser = this.updateProfile.asObservable();
  constructor(private http: HttpClient) {
  }

  editProfile(profile: FormData, urlImg): Observable<User> {
    const user: User = JSON.parse(sessionStorage.getItem(CONST.Local.CURRENT_USER));
    let newProfile: Profile ;
    newProfile = JSON.parse(profile.get('user') as string) ;
    if (urlImg) {
      user.image = urlImg;
    }
    user.address = newProfile.address;
    user.phoneNumber = newProfile.phoneNumber;
    user.lastName = newProfile.lastName;
    user.firstName = newProfile.firstName;
    sessionStorage.removeItem(CONST.Local.CURRENT_USER);
    sessionStorage.setItem(CONST.Local.CURRENT_USER, JSON.stringify(user));
    this.updateProfile.next(user);
    // @ts-ignore
    return this.http.post(this.API_URL_EDIT_PROFILE, profile).pipe();
  }
  updatePass(newPass: NewPassword): Observable<any> {
    return this.http.post(`${this.API_URL_UPDATE_PASSWORD}`, newPass).pipe();
  }

  registerObservable(userRegister: UserRegister): Observable<any> {
    return this.http.post(this.API_URL_REGISTER, userRegister).pipe();
  }
  forgotPassword(email: string): Observable<any> {
    console.log(`${this.API_URL_FORGOT_PASSWORD}${email}`);
    console.log(`${this.API_URL_RESET_PASSWORD}`);
    // @ts-ignore
    return this.http.post(`${this.API_URL_FORGOT_PASSWORD}${email}`).pipe();
  }

  resetPassword(newPassword: ResetPassword, token: string): Observable<any> {
    const httpOption = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
    return this.http.post(`${this.API_URL_RESET_PASSWORD}${token}`, newPassword, httpOption).pipe();
  }
}
