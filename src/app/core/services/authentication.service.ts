import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {User} from '../models/user';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';
import * as CONST from '../constants';
import {CookieService} from 'ngx-cookie-service';
import {CartService} from './cart.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  private readonly API_LOGIN = environment.apiUrl.account.login;

  constructor(private http: HttpClient, private cookieService: CookieService) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(sessionStorage.getItem(CONST.Local.CURRENT_USER)));
    this.currentUser = this.currentUserSubject.asObservable();
  }
  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  login(email: string, password: string) {
    return this.http.post<any>(this.API_LOGIN, {email, password})
      .pipe(map(user => {
        if (user && user.token) {
          sessionStorage.setItem(CONST.Local.CURRENT_USER, JSON.stringify(user));
          this.cookieService.set(CONST.Local.TOKEN, JSON.stringify(user.token));
          this.currentUserSubject.next(user);
        }
        return user;
      }));
  }
  logout() {
    sessionStorage.clear();
    this.cookieService.deleteAll();
    this.currentUserSubject.next(null);
    localStorage.clear();
  }
  isAdmin(): User {
    if (this.currentUserValue) {
      const userRole = this.currentUserValue.roles[0].role;
      if (userRole === 'ADMIN') {
        return this.currentUserValue;
      }
    }
  }
}
