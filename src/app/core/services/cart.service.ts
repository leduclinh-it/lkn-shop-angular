import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {Cart} from '../models/cart';
import * as CONST from '../constants';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {OrderDetail} from '../models/orderDetail';
import {HttpClient} from '@angular/common/http';
import {User} from '../models/user';
import {AuthenticationService} from './authentication.service';



@Injectable({
  providedIn: 'root'
})
export class CartService {
  private readonly API_GET_CART = environment.apiUrl.cart.cart;
  private readonly API_CART_ADD_CART = environment.apiUrl.cart.addCart;
  private readonly API_CART_DELETE_ITEM = environment.apiUrl.cart.changeCart;
  cart: Cart;
  flag: boolean;
  productOrder = null;
  total = 0;
  count;
  private quantity = new BehaviorSubject(JSON.parse(sessionStorage.getItem('quantity')));
  currentQuantity = this.quantity.asObservable();
  currentUser: User;

  constructor(private http: HttpClient, private authenticationService: AuthenticationService) {
    this.authenticationService.currentUser.subscribe(x => { this.currentUser = x; });
  }
  updateQuantity(qtt: number) {
    this.quantity.next(qtt);
  }
  private getLocalCart(): Cart {

    if (sessionStorage.getItem(CONST.Local.CART)) {
      this.cart = JSON.parse(sessionStorage.getItem(CONST.Local.CART));
      return this.cart;
    } else {
      return null;
    }
  }
  getCart(): Observable<Cart> {
    // @ts-ignore
    this.cart = new Cart();
    this.cart.orderDetail = new Array<OrderDetail>();
    this.cart = this.getLocalCart();
    if (this.currentUser) {
      if (this.cart !== null) {
        this.http.post(this.API_GET_CART, this.cart.orderDetail)
          .subscribe(x => {this.clearLocalCart(); });
        // @ts-ignore
        return this.http.get(this.API_GET_CART);
      } else {
        // @ts-ignore
        return this.http.get(this.API_GET_CART);
      }
    } else {
      if (this.getLocalCart() == null) {
        // @ts-ignore
        this.cart = new Cart();
        this.cart.orderDetail = new Array<OrderDetail>();
      }
      return of(this.cart);
    }
  }
  // tslint:disable-next-line:ban-types
  addItemToCart(orderDetail: OrderDetail): Observable<Boolean> {
    // @ts-ignore
    this.cart = new Cart();
    if (this.currentUser) {
      this.productOrder = new OrderDetail();
      this.productOrder = orderDetail;
      // @ts-ignore
      return this.http.post(this.API_CART_ADD_CART, orderDetail).pipe();
    } else {
      if (sessionStorage.getItem(CONST.Local.CART)) {
        this.cart = JSON.parse(sessionStorage.getItem(CONST.Local.CART));
      }
      this.flag = false;
      if (this.cart.orderDetail == null) {
        this.cart.orderDetail = new Array<OrderDetail>();
      }
      this.cart.orderDetail.forEach((x) => {
        if (orderDetail.product.id === x.product.id
          && orderDetail.product.size[0] === x.product.size[0]
          && orderDetail.product.color[0] === x.product.color[0]) {
          x.quanity = x.quanity + orderDetail.quanity;
          x.total = x.product.price * x.quanity;
          this.flag = true;
          this.cart.total = this.cart.total + x.product.price;
        }
      });
      if (!this.flag) {
        orderDetail.total = orderDetail.price * orderDetail.quanity;
        this.cart.orderDetail.push(orderDetail);
        this.cart.total = 0;
        this.cart.orderDetail.forEach(x => {
          this.cart.total += x.total;
        });
      }
      sessionStorage.setItem(CONST.Local.CART, JSON.stringify(this.cart));
      this.count = this.cart.orderDetail.length;
      return of(true);
    }

  }
  // tslint:disable-next-line:ban-types
  removeItem(orderDetail: OrderDetail, index): Observable<Boolean> {
    if (!this.currentUser) {
      this.cart = JSON.parse(sessionStorage.getItem(CONST.Local.CART));
      this.cart.total = this.cart.total - orderDetail.total;
      this.cart.orderDetail.forEach((x) => {
        // tslint:disable-next-line:max-line-length
        if (orderDetail.product.id === x.product.id && orderDetail.product.size[0] === x.product.size[0] && orderDetail.product.color[0] === x.product.color[0]) {
          this.cart.orderDetail.splice(index, 1);
        }
      });
      sessionStorage.setItem(CONST.Local.CART, JSON.stringify(this.cart));
      return of(null);
    } else {
      orderDetail.total = orderDetail.price * orderDetail.quanity;
      // @ts-ignore
      return this.http.post(`${this.API_CART_DELETE_ITEM}`, orderDetail);
    }
  }
  clearLocalCart() {
    sessionStorage.removeItem(CONST.Local.CART);
  }
  // tslint:disable-next-line:ban-types
  updateCart(orderDetail: OrderDetail, index: number) {
    // @ts-ignore
    let cart = new Cart();
    cart.orderDetail = new Array<OrderDetail>();
    cart = JSON.parse(sessionStorage.getItem(CONST.Local.CART));
    for (let i = 0; i < cart.orderDetail.length; i++) {
      if (i === index) {
        cart.orderDetail[i].quanity = orderDetail.quanity;
        cart.orderDetail[i].total = orderDetail.product.price * orderDetail.quanity;
      }
    }
    return sessionStorage.setItem(CONST.Local.CART, JSON.stringify(cart));
  }


}
