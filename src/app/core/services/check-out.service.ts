import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class CheckOutService {

  private readonly API_CART_CHECKOUT = environment.apiUrl.cart.checkout;

  constructor(private http: HttpClient) { }

  checkOut(): Observable<boolean> {
    // @ts-ignore
    return this.http.post(this.API_CART_CHECKOUT, null);
  }

}
