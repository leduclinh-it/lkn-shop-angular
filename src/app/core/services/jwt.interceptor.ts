import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import {AuthenticationService} from './authentication.service';
import {CookieService} from 'ngx-cookie-service';
import * as CONST from '../constants';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

  constructor(private authenticationService: AuthenticationService, private cookieService: CookieService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const currentUser = this.authenticationService.currentUserValue;
    // @ts-ignore
    if (currentUser) {
      const token = JSON.parse(this.cookieService.get(CONST.Local.TOKEN));
      request = request.clone({
          setHeaders: {
            Accept: 'application/json',
            Authorization: `Bearer ${token}`,
          },
        });
    }
    return next.handle(request);
  }
}
