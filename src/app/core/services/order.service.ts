import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {Order} from '../models/order';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  private readonly  API_GET_ORDER_USER = environment.apiUrl.order.orderUser;
  private readonly  API_GET_ORDER_ALL = environment.apiUrl.order.orders;
  private readonly  API_GET_ORDER_STATUS = environment.apiUrl.order.findStatus;
  private readonly  API_GET_ORDER_BY_ID = environment.apiUrl.order.orderById;
  private readonly  API_FINISH_ORDER = environment.apiUrl.order.finish;
  private readonly  API_CANCEL_ORDER = environment.apiUrl.order.cancel;
  private readonly  API_GET_TOTAL_PAGE = environment.apiUrl.order.totalPageUser;
  private readonly  API_GET_PAGE_STATUS = environment.apiUrl.order.getPageStatus;
  private readonly  API_GET_PAGE_ORDER = environment.apiUrl.order.getPage;
  private readonly  API_GET_TOTAL_PAGE_ADMIN = environment.apiUrl.order.totalPage;
  orderCancel: Order;
  orderComplete: Order;

  constructor(private http: HttpClient) { }

  getAllOrder(): Observable<Order[]> {
    // @ts-ignore
    return this.http.get(this.API_GET_ORDER_ALL).pipe();
  }

  getOrderById(id: number): Observable<Order> {
    // @ts-ignore
    return this.http.get(this.API_GET_ORDER_BY_ID + `${id}`).pipe();
  }

  getOrderStatus(status: number): Observable<Order[]> {
    // @ts-ignore
    return this.http.get(`${this.API_GET_ORDER_STATUS}${status}`).pipe();
  }

  getOrderStatusUser(status: number, page: number): Observable<Order[]> {
    // @ts-ignore
    return this.http.get(`${this.API_GET_PAGE_STATUS}/${status}/${page}`).pipe();
  }
  getTotalPage(status: number): Observable<any> {
    return this.http.get(`${this.API_GET_TOTAL_PAGE}${status}`);
  }
  finishOrder(order: Order): Observable<boolean> {
    this.orderComplete = order;
    // @ts-ignore
    return this.http.patch(this.API_FINISH_ORDER + `${order.id}`, null).pipe();
  }

  cancelOrder(order: Order): Observable<boolean> {
    this.orderCancel = order;
    // @ts-ignore
    return this.http.patch(this.API_CANCEL_ORDER + `${order.id}`, null).pipe();
  }
  getPageOrderAdmin(status: number, page: number): Observable<any> {
    return this.http.get(`${this.API_GET_PAGE_ORDER}${status}/${page}`).pipe();
  }
  getTotalOrderAdmin(status: number): Observable<any> {
    return this.http.get(`${this.API_GET_TOTAL_PAGE_ADMIN}${status}`).pipe();
}
}
