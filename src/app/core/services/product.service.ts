import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {Product} from '../models/product';
import {HttpClient} from '@angular/common/http';
import {filter, map} from 'rxjs/operators';
import {CommentBody} from '../models/commentBody';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private readonly API_URL_GET_ALL_PRODUCT = environment.apiUrl.product.products;
  // admin
  private  readonly API_URL_ADD_PRODUCT = environment.apiUrl.product.addProduct;
  private readonly API_URL_DELETE = environment.apiUrl.product.deleteProduct;
  private readonly API_URL_UPDATE = environment.apiUrl.product.editProduct;
  private readonly API_URL_COMMENT_PRODUCT = environment.apiUrl.product.commentProduct;
  private readonly API_URL_DELETE_COMMENT_PRODUCT = environment.apiUrl.product.commentProduct;
  private readonly API_URL_GET_PRODUCT_STATUS = environment.apiUrl.product.statusProduct;
  private readonly API_URL_GET_PRODUCT_PAGE = environment.apiUrl.product.productPage;
  private readonly API_URL_GET_TOTAL_PAGE_BY_STATUS = environment.apiUrl.product.productTotalPage;
  private readonly API_URL_GET_PRODUCT_STATUS_BY_PAGE = environment.apiUrl.product.productStatusPage;


  constructor(private http: HttpClient) { }

  getProducts(): Observable<Product[]> {
    // @ts-ignore
    return this.http.get<Product[]>(this.API_URL_GET_ALL_PRODUCT);
  }
  getProduct(id: number | string) {
    return this.getProducts().pipe(
      map((products) => products.find((product) => product.id === +id))
    );
  }
  getProductByPage(count: number): Observable<object> {
    // @ts-ignore
     return this.http.get(this.API_URL_GET_PRODUCT_PAGE + `${count}`);
  }
  getTotalPageByStatus(status: string): Observable<any> {
    return this.http.get(this.API_URL_GET_TOTAL_PAGE_BY_STATUS + `${status}`);
  }
  getProductsByPage(page: number, status: string): Observable<any> {
    return this.http.get(this.API_URL_GET_PRODUCT_STATUS_BY_PAGE + `${status}/${page}`);
  }

  // admin
  addProduct(form: FormData): Observable<Product> {
    // @ts-ignore
    return this.http.put(this.API_URL_ADD_PRODUCT, form);
  }
  updateProduct(form: FormData): Observable<Product> {
    // @ts-ignore
    return this.http.post(`${this.API_URL_UPDATE}`, form);
  }
  deleteProduct(id: number): Observable<Product> {
    console.log(`${this.API_URL_DELETE}${id}`);
    // @ts-ignore
    return this.http.delete(`${this.API_URL_DELETE}${id}`);
  }
// user
  commentProduct(content: CommentBody): Observable<any> {
    return this.http.put(this.API_URL_COMMENT_PRODUCT, content);
  }
  deleteComment(id: number, index: number): Observable<any> {
    return this.http.delete(this.API_URL_DELETE_COMMENT_PRODUCT + `?idProduct=${id}&indexComment=${index}`);
  }
  // home
  getProductStatus(status: string): Observable<any> {
    // @ts-ignore
    return this.http.get(this.API_URL_GET_PRODUCT_STATUS + `status=${status}`);
  }


}
