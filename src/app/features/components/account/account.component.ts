import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../../core/services/authentication.service';
import {User} from '../../../core/models/user';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NewPassword} from '../../../core/models/newPassword';
import {AccountService} from '../../../core/services/account.service';
import {Profile} from '../../../core/models/profile';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  user: User;
  updatePasswordForm: FormGroup;
  editAccount: FormGroup;
  public imagePath;
  imgURL: any;
  public message: string;
  updatePass: NewPassword;
  error = 0;
  loader = false;
  loaderUser = false;
  constructor(private authenticationService: AuthenticationService,
              private formBuilder: FormBuilder,
              private accountService: AccountService) { }

  ngOnInit(): void {
    this.getUser();
    this.editAccount = this.formBuilder.group({
      firstName: [this.user.firstName, Validators.required],
      lastName: [this.user.lastName, Validators.required],
      phoneNumber: [this.user.phoneNumber, Validators.required],
      address: [this.user.address, Validators.required]
    });
    this.updatePasswordForm = this.formBuilder.group({
      newPassword: ['', Validators.required],
      rePassword: ['', Validators.required],
      oldPassword: ['', Validators.required]
    });
  }

  getUser() {
   return this.user = JSON.parse(sessionStorage.getItem('currentUser'));
  }
  onSelectFile(files) {
    if (files.length === 0) {
      return;
    }
    const mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = 'Only images are supported.';
      return;
    }
    const reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    // tslint:disable-next-line:variable-name
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    };
  }
  get p() {
    return this.updatePasswordForm.controls;
  }
  cancel() {
    this.imgURL = null;
  }
  get f() {return this.editAccount.controls; }
  updateProfile() {
    // tslint:disable-next-line:max-line-length
    // const user = new User();
    this.loaderUser = true;
    const fromData = new FormData();
    const updateProfile = new Profile();
    updateProfile.firstName = this.f.firstName.value;
    updateProfile.lastName = this.f.lastName.value;
    updateProfile.phoneNumber = this.f.phoneNumber.value;
    updateProfile.address = this.f.address.value;
    if (this.imgURL) {
      fromData.append('file', this.imagePath[0]);
     }
    if (!this.imgURL) {
      fromData.append('file', this.user.image);
    }

    fromData.append('user', JSON.stringify(updateProfile));
    this.accountService.editProfile(fromData, this.imgURL).subscribe(() => {
      this.imgURL = null;
      this.ngOnInit();
      this.loaderUser = false;
    });
  }
  checkPasswords(account) { // here we have the 'passwords' group
    const pass = account.get('newPassword').value;
    const confirmPass = account.get('rePassword').value;
    return pass === confirmPass ? null : { notSame: true };
  }
  onChangePassword(): void {
    this.loader = true;
    this.updatePass = new NewPassword(this.p.oldPassword.value, this.p.newPassword.value);
    this.accountService.updatePass(this.updatePass).subscribe((data) => {
      this.error = data.code;
      if (this.error === 200) {
        this.loader = false;
        this.ngOnInit();
      }
    });
  }
}
