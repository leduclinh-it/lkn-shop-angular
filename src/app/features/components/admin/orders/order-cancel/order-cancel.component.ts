import { Component, OnInit } from '@angular/core';
import {OrderService} from '../../../../../core/services/order.service';

@Component({
  selector: 'app-order-cancel',
  templateUrl: './order-cancel.component.html',
  styleUrls: ['./order-cancel.component.scss']
})
export class OrderCancelComponent implements OnInit {

  orders;
  loader = true;
  listPage: number[] = [];
  pageNumber: number;
  constructor(private orderService: OrderService) { }
  ngOnInit(): void {
    this.getTotalPage();
    this.getOrderPage(0);
  }
  getTotalPage() {
    this.orderService.getTotalOrderAdmin(2).subscribe(x => {
      for (let i = 0; i < x ; i++) {
        this.listPage.push(i);
      }
    });
  }
  getOrderPage(page: number) {
    this.pageNumber = page;
    this.orders = [];
    this.loader = true;
    this.orderService.getPageOrderAdmin(2, page).subscribe(x => {
      this.orders = x;
      this.loader = false;
    });
  }
  selectPage(i: number) {
    this.getOrderPage(i);
  }
}
