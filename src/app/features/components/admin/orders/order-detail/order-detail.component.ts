import { Component, OnInit } from '@angular/core';
import {OrderService} from '../../../../../core/services/order.service';
import {Order} from '../../../../../core/models/order';
import {ActivatedRoute, Router} from '@angular/router';
import {User} from '../../../../../core/models/user';
import {OrderDetail} from '../../../../../core/models/orderDetail';

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.scss']
})
export class OrderDetailComponent implements OnInit {

  orderDetail: Order;
  loader = true;
  total: number;
  status: number;
  statusParam: string;
  constructor(private orderService: OrderService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.getOrder();
  }
  getOrder(): void {
    this.orderDetail = new Order();
    this.orderDetail.custommer = new User();
    this.orderDetail.details = new Array<OrderDetail>();
    const id = +this.route.snapshot.paramMap.get('id');
    this.statusParam = this.route.snapshot.paramMap.get('status');
    this.orderService.getOrderById(id).subscribe(data => {
      this.orderDetail = data;
      this.total =  this.orderDetail.total;
      this.loader = false;
      this.status = this.orderDetail.oderStatus;
      console.log(this.status);
    });
  }
  close() {
    this.router.navigate(['/admin/' + this.statusParam]);
  }
  completeOrder(order: Order) {
    this.orderService.finishOrder(order).subscribe(data => {
      this.router.navigate(['/admin/' + this.statusParam]);
    });
  }

  cancelOrder(order: Order) {
    this.orderService.cancelOrder(order).subscribe(data => {
      this.router.navigate(['/admin/' + this.statusParam]);
    });
  }
}
