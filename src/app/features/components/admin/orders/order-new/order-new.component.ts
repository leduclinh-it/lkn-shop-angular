import { Component, OnInit } from '@angular/core';
import {OrderService} from '../../../../../core/services/order.service';
import {Order} from '../../../../../core/models/order';


@Component({
  selector: 'app-order-new',
  templateUrl: './order-new.component.html',
  styleUrls: ['./order-new.component.scss']
})
export class OrderNewComponent implements OnInit {

  orders: Order[];
  loader = true;
  listPage: number[] = [];
  pageNumber = 0;
  constructor(private orderService: OrderService) { }

  ngOnInit(): void {
    this.getTotalPage();
    this.getOrderPage(this.pageNumber);
  }
  completeOrder(order: Order, index) {
    this.orderService.finishOrder(order).subscribe(data => {
      this.orders.splice(index, 1);
    });
  }

  cancelOrder(order: Order, index) {
    this.orderService.cancelOrder(order).subscribe(data => {
      this.orders.splice(index, 1);
    });
  }
  getTotalPage() {
    this.orderService.getTotalOrderAdmin(0).subscribe(x => {
      for (let i = 0; i < x ; i++) {
        this.listPage.push(i);
      }
    });
  }
  getOrderPage(page: number) {
    this.pageNumber = page;
    this.orders = [];
    this.loader = true;
    this.orderService.getPageOrderAdmin(0, page).subscribe(x => {
      this.orders = x;
      this.loader = false;
    });
  }
  selectPage(i: number) {
    this.getOrderPage(i);
  }

}
