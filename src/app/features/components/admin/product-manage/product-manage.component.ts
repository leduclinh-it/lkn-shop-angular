import {Component, OnInit} from '@angular/core';
import {ProductService} from '../../../../core/services/product.service';
import {Product} from '../../../../core/models/product';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-product-manage',
  templateUrl: './product-manage.component.html',
  styleUrls: ['./product-manage.component.scss']
})
export class ProductManageComponent implements OnInit {

  products: Product[];
  totalRecords: number;
  product: Product;
  formProduct: FormGroup;
  colors = ['Xanh', 'Đỏ', 'Vàng'];
  sizes = ['XS', 'S', 'ML', 'XL', 'XXL'];
  status = ['sale', 'new', 'sold-out', 'hot'];
  public imagePath;
  imgURL: any;
  public message: string;
  selectedProduct: Product;
  loader = false;
  totalPage: number;
  listPage: number[] = [];
  pageNumber = 0;
  page;


  constructor(private productService: ProductService, private formBuilder: FormBuilder) {
    this.createForm();
  }

  ngOnInit(): void {
    this.getProductByPage(0);
  }
  createForm() {
    this.formProduct = this.formBuilder.group({
      nameProduct: ['', Validators.required],
      priceProduct: ['', Validators.required],
      colorProduct: [[], Validators.required],
      brandProduct: ['', Validators.required],
      sizeProduct: [[], Validators.required],
      statusProduct: [[], Validators.required],
      descriptionProduct: ['', Validators.required]
    });
  }
  get nameProduct() { return this.formProduct.get('nameProduct'); }
  get priceProduct() { return this.formProduct.get('priceProduct'); }
  get colorProduct() { return this.formProduct.get('colorProduct'); }
  get brandProduct() { return this.formProduct.get('brandProduct'); }
  get sizeProduct() { return this.formProduct.get('sizeProduct'); }
  get statusProduct() { return this.formProduct.get('sizeProduct'); }
  get descriptionProduct() { return this.formProduct.get('descriptionProduct'); }
  onAddProduct() {
    this.loader = true;
    const newProduct = new Product();
    newProduct.name = this.formProduct.get('nameProduct').value;
    newProduct.price = Number(this.formProduct.get('priceProduct').value);
    newProduct.color = this.formProduct.get('colorProduct').value;
    newProduct.brand = this.formProduct.get('brandProduct').value;
    newProduct.size = this.formProduct.get('sizeProduct').value;
    newProduct.status = this.formProduct.get('statusProduct').value;
    newProduct.description = this.formProduct.get('descriptionProduct').value;
    newProduct.image[0] = this.imgURL;
    this.products.push(newProduct);
    this.totalRecords = this.products.length;
    const fromData = new FormData();
    console.log(this.imagePath);
    fromData.append('file', this.imagePath[0]);
    fromData.append('product', JSON.stringify(newProduct));
    return this.productService.addProduct(fromData).subscribe(x => {
      this.loader = false;
    });
  }
  clearFrom() {
    this.imgURL = '';
    this.nameProduct.setValue('');
    this.priceProduct.setValue('');
    this.colorProduct.setValue('');
    this.brandProduct.setValue('');
    this.sizeProduct.setValue('');
    this.statusProduct.setValue('');
    this.descriptionProduct.setValue('');
  }
  onDelete(id: number, index: number) {
    this.loader = true;
    const count = index;
    this.products.splice(count, 1);
    this.totalRecords = this.products.length;
    this.productService.deleteProduct(id).subscribe((data: Product) => {
      this.loader = false;
    });
  }
  onSelectFile(files) {
    if (files.length === 0) {
      return;
    }
    const mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = 'Only images are supported.';
      return;
    }
    const reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    // tslint:disable-next-line:variable-name
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    };
  }
  selectProduct(product: Product) {
    this.selectedProduct = product;
    this.formProduct.get('nameProduct').setValue(this.selectedProduct.name);
    this.formProduct.get('priceProduct').setValue(this.selectedProduct.price);
    this.formProduct.get('colorProduct').setValue(this.selectedProduct.color);
    this.formProduct.get('brandProduct').setValue(this.selectedProduct.brand);
    this.formProduct.get('sizeProduct').setValue(this.selectedProduct.size);
    this.formProduct.get('statusProduct').setValue(this.selectedProduct.status);
    this.formProduct.get('descriptionProduct').setValue(this.selectedProduct.description);
    this.imgURL = this.selectedProduct.image[0];
  }
  onUpdate() {
    this.loader = true;
    this.selectedProduct.name = this.formProduct.get('nameProduct').value;
    this.selectedProduct.price = this.formProduct.get('priceProduct').value;
    this.selectedProduct.color = this.formProduct.get('colorProduct').value;
    this.selectedProduct.brand = this.formProduct.get('brandProduct').value;
    this.selectedProduct.size = this.formProduct.get('sizeProduct').value;
    this.selectedProduct.status = this.formProduct.get('statusProduct').value;
    this.selectedProduct.description = this.formProduct.get('descriptionProduct').value;
    this.selectedProduct.image[0] = this.imgURL;
    const fromData = new FormData();
    fromData.append('file', this.selectedProduct.image[0]);
    fromData.append('product', JSON.stringify(this.selectedProduct));
    this.productService.updateProduct(fromData).subscribe((data) => {
      this.loader = false;
    });
    this.clickCancel();
  }
  clickCancel() {
    this.selectedProduct = null;
    this.formProduct.get('nameProduct').setValue('');
    this.formProduct.get('priceProduct').setValue('');
    this.formProduct.get('colorProduct').setValue('');
    this.formProduct.get('brandProduct').setValue('');
    this.formProduct.get('sizeProduct').setValue('');
    this.formProduct.get('descriptionProduct').setValue('');
  }
  deleteImage() {
    this.imgURL = null;
  }
  getProductByPage(page: number) {
    this.loader = true;
    // @ts-ignore
    // tslint:disable-next-line:no-unused-expression
    this.productService.getProductByPage(page).subscribe(x => {
      this.pageNumber = page;
      this.page = x;
      this.products = this.page.content;
      this.totalPage = this.page.totalPages;
      this.loader = false;
      for (let i = 0 ; i < this.totalPage ; i++ ) {
        if (this.listPage.length < this.totalPage ) {
          this.listPage.push(i);
        } else {
          return null;
        }
      }
    });
  }


}
