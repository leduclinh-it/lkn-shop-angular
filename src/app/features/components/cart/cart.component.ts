import {Component, OnInit} from '@angular/core';
import {CartService} from '../../../core/services/cart.service';
import {Cart} from '../../../core/models/cart';
import {OrderDetail} from '../../../core/models/orderDetail';
import {AuthenticationService} from '../../../core/services/authentication.service';
import {Router} from '@angular/router';
import {User} from '../../../core/models/user';
import {Observable} from 'rxjs';
import {Product} from '../../../core/models/product';
import {ProductService} from '../../../core/services/product.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  cart: Cart ;
  loader = true;
  total: number;
  currentUser: User;
  products;
  flag = false;
  quantity: number;
  // tslint:disable-next-line:max-line-length
  constructor(private cartService: CartService,
              private authenticationService: AuthenticationService,
              private route: Router,
              private productService: ProductService) {
    this.cart = new Cart(new Array<OrderDetail>(), 0);

  }

  ngOnInit(): void {
    this.getProducts();
    this.getCart();
    this.authenticationService.currentUser.subscribe(x => {this.currentUser = x; });
  }
  // @ts-ignore
  getProducts(): Observable<Product[]> {
    this.loader = true;
    // @ts-ignore
    this.productService.getProducts().subscribe(data => {this.products = data; this.loader = false; });
  }
  getCart() {
    this.loader = true;
    this.cartService.getCart().subscribe((data) => {
      if (!data) {
        data = new Cart(new Array<OrderDetail>(), 0);
      }
      this.cart = data;
      if (this.cartService.productOrder !== null) {
        const product = this.cartService.productOrder;
        this.cart.orderDetail.map(x => {
          if (product.product.color.toString() === x.product.color.toString()
            && product.product.size.toString() === x.product.size.toString()
            && product.product.id === x.product.id
          ) {
            x.quanity = x.quanity + product.quanity;
            x.total = x.total + (product.quanity * product.product.price);
            this.flag = true;
          }
        });
        if (this.flag === false) {
          this.cart.orderDetail.push(this.cartService.productOrder);
        }
        this.cartService.productOrder = null;
      }
      this.quantity = this.cart.orderDetail.length;
      this.cart.total = 0;
      this.cart.orderDetail.map(x => {
        this.cart.total += x.total;
      });
      this.loader = false;
      sessionStorage.setItem('quantity', JSON.stringify(this.quantity));
      this.cartService.updateQuantity(this.quantity);
    });
  }
  removeItem(product: OrderDetail, index) {
    this.cartService.removeItem(product, index).subscribe(x => {
      this.cart.orderDetail.splice(index, 1);
      this.cart.total = this.cart.total - product.total;
      this.quantity = this.cart.orderDetail.length;
      sessionStorage.setItem('quantity', JSON.stringify(this.quantity));
      this.cartService.updateQuantity(this.quantity);
    });
  }

  updateQTY(status: string, product: OrderDetail, index: number) {
    const addItem = () => {
      this.loader = true;
      this.cartService.addItemToCart(product).subscribe(x => {
        this.cartService.productOrder = null;
        updateTotal();
      });
    };
    if (product.quanity !== 1) {
        if (status === '-') {
          product.quanity = product.quanity - 1;
          if (this.currentUser) {
            this.cartService.removeItem(product, index).subscribe(n  => {
               addItem();
            });
          } else {
            this.cartService.updateCart(product, index);
            this.getCart();
          }
        }
    }
    if (product.quanity === 1) {
      console.log('ok');
      if (status === '-') {
        this.cartService.removeItem(product, index).subscribe(
          () => {this.cart.orderDetail.splice(index, 1); this.getCart(); }
      );
      }
    }
    if (status === '+') {
      product.quanity = product.quanity + 1;
      if (this.currentUser) {
          this.cartService.removeItem(product, index).subscribe(n  => {
            addItem();
          });
        } else {
          this.cartService.updateCart(product, index);
          this.getCart();
        }
    }
    const updateTotal = () => {
      this.cart.total = 0;
      this.cart.orderDetail.map(x => {
        this.cart.total += x.total;
        this.loader = false;
      });
    };
  }
  onCheckOut() {
      if (this.currentUser) {
        this.route.navigate(['/check-out']);
      } else {
        this.route.navigate(['/login']);
      }
  }
}
