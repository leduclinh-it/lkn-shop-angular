import { Component, OnInit } from '@angular/core';
import {CartService} from '../../../core/services/cart.service';
import {Cart} from '../../../core/models/cart';
import {CheckOutService} from '../../../core/services/check-out.service';
import {OrderDetail} from '../../../core/models/orderDetail';
import {AuthenticationService} from '../../../core/services/authentication.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {User} from '../../../core/models/user';
import {Router} from '@angular/router';

@Component({
  selector: 'app-check-out',
  templateUrl: './check-out.component.html',
  styleUrls: ['./check-out.component.scss']
})
export class CheckOutComponent implements OnInit {

  cart: Cart;
  infoUser: FormGroup;
  user: User;
  checkedOld = true;
  checkedNew = false;
  constructor(private cartService: CartService,
              private checkOutService: CheckOutService,
              private authenticationService: AuthenticationService,
              private formBuilder: FormBuilder,
              private route: Router) { }

  ngOnInit(): void {
    this.getUser();
    this.getCart();
    this.infoUser = this.formBuilder.group({
      userName: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      address: ['', Validators.required]
    });
  }

  getCart() {
    // @ts-ignore
    this.cart = new Cart();
    this.cart.orderDetail = new Array<OrderDetail>();
    return this.cartService.getCart().subscribe(data => this.cart = data);
  }
  getUser() {
    return this.user = this.authenticationService.currentUserValue;
  }


  onCheckOut() {

    if (this.infoUser.status === 'INVALID' || this.checkedNew) {
      this.checkOutService.checkOut().subscribe(x => {
        this.route.navigate(['/cart']);
      });
    }
  }
  onCheckedOld(e) {
    this.checkedOld = e.target.checked;
    this.checkedNew = ! this.checkedOld;
  }
  onCheckedNew(e) {
    this.checkedNew = e.target.checked;
    this.checkedOld = !this.checkedNew;
  }



}
