import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AccountService} from '../../../core/services/account.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NewPassword} from '../../../core/models/newPassword';
import {ResetPassword} from '../../../core/models/resetPassword';

@Component({
  selector: 'app-confirm-reset',
  templateUrl: './confirm-reset.component.html',
  styleUrls: ['./confirm-reset.component.scss']
})
export class ConfirmResetComponent implements OnInit {

  formChangePassword: FormGroup;
  token;
  // tslint:disable-next-line:max-line-length
  constructor(private formBuilder: FormBuilder, private accountService: AccountService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.formChangePassword = this.formBuilder.group({
      newPass: ['', Validators.required],
      oldPass: ['', Validators.required]
    });
  }
  get newPassword() { return this.formChangePassword.get('newPass'); }
  get oldPassword() {return this.formChangePassword.get('oldPass'); }
  checkPasswords() { // here we have the 'passwords' group
    const pass = this.newPassword.value;
    const confirmPass = this.oldPassword.value;
    return pass === confirmPass ? null : { notSame: true };
  }
  onSubmitNewPassword() {
    // @ts-ignore
    this.route.queryParams.subscribe(params => {
      this.token = params.token;
    });
    const newPassword = new ResetPassword(this.newPassword.value);
    this.accountService.resetPassword(newPassword, this.token).subscribe(x => {
      if (x.code === 200) {
        this.router.navigate(['/login']);
      } else {
        alert('loi');
      }
    });
  }

}
