import { Component, OnInit } from '@angular/core';
import {AccountService} from '../../../core/services/account.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-email-confirm',
  templateUrl: './email-confirm.component.html',
  styleUrls: ['./email-confirm.component.scss']
})
export class EmailConfirmComponent implements OnInit {

  emailConfirm: FormGroup;
  error;
  loader = false;
  constructor(private accountService: AccountService, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.emailConfirm = this.fb.group({
      emailAddress: ['', [Validators.required, Validators.email]]
    });
  }
  get email() { return this.emailConfirm.get('emailAddress'); }

  sendMess() {
      this.loader = true;
      this.error = 0;
      this.accountService.forgotPassword(this.email.value).subscribe(x => {
        console.log(x);
        this.loader = false;
        this.error = x.code;
      } );
  }
}
