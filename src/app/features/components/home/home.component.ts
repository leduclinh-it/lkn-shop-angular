import { Component, OnInit } from '@angular/core';
import {ProductService} from '../../../core/services/product.service';
import {Product} from '../../../core/models/product';
import {Observable} from 'rxjs';
import {CartService} from '../../../core/services/cart.service';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  products: Product[];
  productsLast: Product[];
  productHot: Product[];
  productNew: Product[];
  productSale: Product[];
  constructor(private productService: ProductService, private cartService: CartService) { }

  ngOnInit(): void {
    this.getProducts();
    this.getProductLast();
    this.getProductStatus();
  }
  // @ts-ignore
  getProducts(): Observable<Product[]> {
    // @ts-ignore
    this.productService.getProducts().subscribe(data => this.products = data);
  }
  getProductLast() {
   this.productsLast = this.products;
  }
  // @ts-ignore
  getProductStatus(): Observable<Product[]> {
     this.productService.getProductStatus('new').subscribe(x => {
       this.productNew = x;
     });
     this.productService.getProductStatus('hot').subscribe(x => {
       this.productHot = x;
     });
     this.productService.getProductStatus('sale').subscribe(x => {
       this.productSale = x;
     });

  }
}
