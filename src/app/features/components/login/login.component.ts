import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService} from '../../../core/services/authentication.service';
import {Router} from '@angular/router';
import {CartService} from '../../../core/services/cart.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loader = false;
  error;

  constructor(private authenticationService: AuthenticationService, private route: Router, private cartService: CartService) { }

  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required])
    });
  }

  get email() { return this.loginForm.get('email'); }
  get password() { return this.loginForm.get('password'); }

  onSubmit() {
    this.loader = true;
    this.error = 0;
    this.authenticationService.login(this.email.value, this.password.value)
      .subscribe(user => {
        this.loader = false;
        this.error = user.code;
        if (user.code !== 20002 && user.code !== 20001) {
          this.route.navigate(['/home']);
          this.cartService.getCart();
        }
    });
  }

}
