import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderUserDetailComponent } from './order-user-detail.component';

describe('OrderUserDetailComponent', () => {
  let component: OrderUserDetailComponent;
  let fixture: ComponentFixture<OrderUserDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderUserDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderUserDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
