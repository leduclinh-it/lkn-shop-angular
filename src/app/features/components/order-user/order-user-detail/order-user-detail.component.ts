import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {OrderService} from '../../../../core/services/order.service';
import {Order} from '../../../../core/models/order';
import {User} from '../../../../core/models/user';
import {OrderDetail} from '../../../../core/models/orderDetail';

@Component({
  selector: 'app-order-user-detail',
  templateUrl: './order-user-detail.component.html',
  styleUrls: ['./order-user-detail.component.scss']
})
export class OrderUserDetailComponent implements OnInit {

  order: Order;
  user: User;
  orderDetails: OrderDetail[];
  total = 0;
  @Input() orderUser: Order;
  @Input() status: number;
  // tslint:disable-next-line:no-output-on-prefix
  @Output('cancel') onInit = new EventEmitter<number>();
  constructor(private orderService: OrderService) { }

  ngOnInit(): void {
    this.user = this.orderUser.custommer;
    this.orderDetails = this.orderUser.details;
    this.orderDetails.map(x => {
      this.total += x.total;
    });
  }
  cancelOrder() {
    this.orderService.cancelOrder(this.orderUser).subscribe(x => {
        this.onInit.emit(0);
    });
  }
  close() {
    if (this.status === 0) {
      this.onInit.emit(0);
    }
    if (this.status === 1) {
      this.onInit.emit(1);
    }
    if (this.status === 2) {
      this.onInit.emit(2);
    }
  }

}
