import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderUserStatusComponent } from './order-user-status.component';

describe('OrderUserStatusComponent', () => {
  let component: OrderUserStatusComponent;
  let fixture: ComponentFixture<OrderUserStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderUserStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderUserStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
