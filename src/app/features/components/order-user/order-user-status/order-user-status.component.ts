import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Order} from '../../../../core/models/order';
import {OrderService} from '../../../../core/services/order.service';

@Component({
  selector: 'app-order-user-status',
  templateUrl: './order-user-status.component.html',
  styleUrls: ['./order-user-status.component.scss']
})
export class OrderUserStatusComponent implements OnInit {

  status: number;
  constructor() { }

  @Input() orderUser;
  @Input()  listPage;
  @Input() pageNumber;
  // tslint:disable-next-line:no-output-rename
  @Output('selectedOrder') selectedOrder = new EventEmitter<Order>();
  // tslint:disable-next-line:no-output-native
  @Output('select') selectPageNumber = new EventEmitter<number>();
  ngOnInit(): void {
  }
  selectOrder(order: Order) {
    this.selectedOrder.emit(order);
  }
  selectPage(i: number) {
    this.selectPageNumber.emit(i);
  }

}
