import { Component, OnInit } from '@angular/core';
import {OrderService} from '../../../core/services/order.service';
import {Order} from '../../../core/models/order';
import {User} from '../../../core/models/user';
import {OrderDetail} from '../../../core/models/orderDetail';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-order-user',
  templateUrl: './order-user.component.html',
  styleUrls: ['./order-user.component.scss']
})
export class OrderUserComponent implements OnInit {

  orderUser: Order[];
  order: Order;
  user: User;
  total = 0;
  new = false;
  cancel = false;
  complete = false;
  loading = false;
  status: number;
  totalPage: number;
  listPage: number[] = [];
  pageNumber = 0;
  constructor(private orderService: OrderService, private route: Router, private router: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.getOrderNew(this.pageNumber);
  }
  getOrderNew(page: number) {
    this.route.navigate(['/order-user/new']);
    this.listPage = [];
    this.loading = true;
    this.orderService.getTotalPage(0).subscribe(total => {
      this.totalPage = total;
      for (let i = 0; i < this.totalPage ; i++) {
        this.listPage.push(i);
      }
      this.orderService.getOrderStatusUser(0, page).subscribe(orders => {
        if (!orders) {
          orders = new Array<Order>();
          orders[0].details = new Array<OrderDetail>();
        }
        this.orderUser = orders;
        this.loading = false;
      });
    });
    this.order = null;
    this.new = true;
    this.cancel = false;
    this.complete = false;
  }
  getOrderCancel(page: number) {
    this.route.navigate(['/order-user/cancel']);
    this.orderUser = null;
    this.loading = true;
    this.listPage = [];
    this.orderService.getTotalPage(2).subscribe(total => {
      this.totalPage = total;
      for (let i = 0; i < this.totalPage ; i++) {
        this.listPage.push(i);
      }
      this.orderService.getOrderStatusUser(2, page).subscribe(orders => {
        if (!orders) {
          orders = new Array<Order>();
          orders[0].details = new Array<OrderDetail>();
        }
        this.orderUser = orders;
        this.loading = false;
      });
    });
    this.order = null;
    this.new = false;
    this.cancel = true;
    this.complete = false;
  }
  getOrderComplete(page: number) {
    this.route.navigate(['/order-user/complete']);
    this.listPage = [];
    this.orderUser = null;
    this.loading = true;
    this.orderService.getTotalPage(2).subscribe(total => {
      this.totalPage = total;
      for (let i = 0; i < this.totalPage ; i++) {
        this.listPage.push(i);
      }
      this.orderService.getOrderStatusUser(1, page).subscribe(orders => {
        if (!orders) {
          orders = new Array<Order>();
          orders[0].details = new Array<OrderDetail>();
        }
        this.orderUser = orders;
        this.loading = false;
      });
    });
    this.order = null;
    this.new = false;
    this.cancel = false;
    this.complete = true;
  }
  getOrder(value) {
    this.order = value;
    this.status = value.orderStatus;
  }
  onInIt(value) {
    if (value === 0) {
      this.ngOnInit();
    }
    if (value === 1) {
      this.getOrderComplete(0);
    }
    if (value === 2) {
      this.getOrderCancel(0);
    }
  }
  getPage(value) {
    const status = this.router.snapshot.paramMap.get('status');
    this.pageNumber = value;
    if (status === 'new') {
      this.getOrderNew(this.pageNumber);
    }
    if (status === 'cancel') {
      this.getOrderCancel(this.pageNumber);
    }
    if (status === 'complete') {
      this.getOrderComplete(this.pageNumber);
    }
  }


}
