import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Product} from '../../../../core/models/product';
import {ProductService} from '../../../../core/services/product.service';
import {CartService} from '../../../../core/services/cart.service';
import {OrderDetail} from '../../../../core/models/orderDetail';
import {Cart} from '../../../../core/models/cart';
import {Comment} from '../../../../core/models/comment';
import {User} from '../../../../core/models/user';
import {CommentBody} from '../../../../core/models/commentBody';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService} from '../../../../core/services/authentication.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

  product: Product;
  cart: Cart;
  error = false;
  loader = false;
  comments: Comment[];
  content: string;
  index: number;
  products;

  constructor(private route: ActivatedRoute,
              private productService: ProductService,
              ) {
  }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.loader = true;
    this.productService.getProduct(id).subscribe((product) => {
      this.product = product;
      this.comments = this.product.commentProducts;
      this.loader = false;
    });

    this.getProducts();
  }
  // @ts-ignore
  getProducts(): Observable<Product[]> {
    // @ts-ignore
    this.productService.getProducts().subscribe(data => this.products = data);
  }
  onselectProduct(id: number) {
    this.productService.getProduct(id).subscribe(x => {this.product = x; });
  }



}
