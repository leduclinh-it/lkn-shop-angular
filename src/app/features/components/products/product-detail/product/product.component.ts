import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Product} from '../../../../../core/models/product';
import {OrderDetail} from '../../../../../core/models/orderDetail';
import {Comment} from '../../../../../core/models/comment';
import {CommentBody} from '../../../../../core/models/commentBody';
import {User} from '../../../../../core/models/user';
import {CartService} from '../../../../../core/services/cart.service';
import {ProductService} from '../../../../../core/services/product.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService} from '../../../../../core/services/authentication.service';
import {ModalDirective} from 'angular-bootstrap-md';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  currentUser: User = null;
  selectedColor = [];
  selectedSize = [];
  error = false;
  quantity = 1;
  dateNow: Date;
  loadCMT = false;
  content;
  selectContent: string;
  loader = false;
  loginForm: FormGroup;
  er;


  @Input() product: Product;
  @ViewChild('login') mdbModal: ModalDirective;
  constructor(
    private cartService: CartService,
    private productService: ProductService,
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
  ) {
    this.authenticationService.currentUser.subscribe(x => {
      this.currentUser = x;
    });
  }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required])
    });
  }
  addToCart(product: Product) {

    const item = new Product();
    if (this.selectedColor.length === 0 || this.selectedSize.length === 0) {
      this.error = true;
    }
    if (this.selectedSize.length !== 0 && this.selectedColor.length !== 0) {
      // @ts-ignore
      item.size.push(this.selectedSize);
      // @ts-ignore
      item.color.push(this.selectedColor);
      product.size = item.size;
      product.color = item.color;
      this.error = false;
      const orderDetail = new OrderDetail(product, this.quantity, product.price, this.quantity * product.price);
      this.cartService.addItemToCart(orderDetail).subscribe(x => {
      });
      this.router.navigate(['/cart']);
    }

  }

  onComment() {
    this.loadCMT = true;
    this.dateNow = new Date(Date.now());
    const comment = new Comment(this.dateNow, this.currentUser.id, this.currentUser.image, this.currentUser.username, this.content);
    const contentProduct = new CommentBody(this.product.id, comment);
    this.productService.commentProduct(contentProduct).subscribe(data => {
      this.product.commentProducts.push(comment);
      this.content = null;
      this.loadCMT = false;
    });
  }
  deleteComment(index: number) {
    let idUser;
    for (let i = 0; i < this.product.commentProducts.length; i++) {
      if (i === index) {
        idUser = this.product.commentProducts[i].idUser;
      }
    }
    if (this.currentUser.id === idUser) {
      this.productService.deleteComment(this.product.id, index).subscribe(data => {
        for (let i = 0; i < this.product.commentProducts.length; i++) {
          if (i === index) {
            this.product.commentProducts.splice(i, 1);
            break;
          }
        }
      });
    } else {
      return null;
    }
  }
  get f() { return this.loginForm.controls; }
  get email() { return this.loginForm.get('email'); }
  get password() { return this.loginForm.get('password'); }

  onSubmit() {
    this.loader = true;
    this.authenticationService.login(this.f.email.value, this.f.password.value)
      .subscribe(user => {
        this.er = user.code;
        if (user.code !== 20002 && user.code !== 20001) {
          this.ngOnInit();
          this.mdbModal.hide();
          this.loader = false;
        }
      });
  }

}
