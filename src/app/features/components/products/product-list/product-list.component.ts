import { Component, OnInit } from '@angular/core';
import {Product} from '../../../../core/models/product';
import {ProductService} from '../../../../core/services/product.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  products: Product[];
  status = 'all';
  page;
  productName: string;
  loader = true;
  totalPage: number;
  listPage: number[] = [];
  pageNumber = 0;
  constructor(private productService: ProductService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    const status = this.route.snapshot.paramMap.get('status');
    if (status !== 'all') {
      this.status = status;
      this.getProductOrderBy(status);
      this.loader = false;
    } else {
      this.getProductByPage(0);
    }
  }

  getTotalPage(status: string) {
    this.productService.getTotalPageByStatus(status).subscribe(x => {
      this.totalPage = x;
      for (let i = 0; i < x ; i++) {
        this.listPage.push(i);
      }
    });
  }
  getProductOrderBy(status: string) {
    this.listPage = [];
    this.status = status;
    if (status === 'all') {
      this.router.navigate(['/product-list/all']);
      return  this.getProductByPage(0);
    } else  {
      return this.productService.getProductStatus(status).subscribe(x => {
        this.products = x;
        this.router.navigate([`/product-list/${status}`]);
        this.getTotalPage(status);
        this.pageNumber = 0;
      });
    }
  }
  getProductByPage(page: number) {
    this.loader = true;
    this.pageNumber = page;
    if (this.status === 'all') {
      // @ts-ignore
      // tslint:disable-next-line:no-unused-expression
      this.productService.getProductByPage(this.pageNumber).subscribe(x => {
        this.page = x;
        this.products = this.page.content;
        this.totalPage = this.page.totalPages;
        this.loader = false;
        for (let i = 0 ; i < this.totalPage ; i++ ) {
          if (this.listPage.length < this.totalPage ) {
            this.listPage.push(i);
          } else {
            return null;
          }
        }
      });
    } else {
        this.productService.getProductsByPage(this.pageNumber, this.status).subscribe(x => {
          console.log(this.pageNumber);
          this.products = x;
        });
    }
  }

}
