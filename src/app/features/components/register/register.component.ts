import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserRegister} from '../../../core/models/userRegister';
import {AccountService} from '../../../core/services/account.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  public userRegister: UserRegister;
  lastName;
  firstName;
  error;
  constructor(private fb: FormBuilder, private accountService: AccountService, private route: Router) { }

  ngOnInit(): void {
    this.registerForm = this.fb.group({
      fullName : ['', Validators.required],
      emailAddress: ['', [Validators.required, Validators.email]],
      phoneNumber: ['', [Validators.required, Validators.maxLength(11) , Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$')]],
      password: ['', Validators.required],
      rePassword: ['', [Validators.required]]
    });
  }

  splitFullName(name) {
    const fullName = name.split(' ', );
    this.lastName = fullName[0];
    let firstName = '';
    // tslint:disable-next-line:prefer-for-of
    for (let i = 1; i < fullName.length; i ++) {
      firstName += fullName[i].toString() + ' ';
    }
    // this.currentUser.firstName = firstName;
    // this.currentUser.lastName = lastName;
    this.firstName = firstName;
    console.log('first name: ' + this.firstName);
    console.log('last name: ' + this.lastName);
  }
  checkPasswords() { // here we have the 'passwords' group
    const pass = this.f.password.value;
    const confirmPass = this.f.rePassword.value;
    return pass === confirmPass ? null : { notSame: true };
  }

  get f() { return this.registerForm.controls; }
  onSubmit(): void {
    // tslint:disable-next-line:max-line-length
    this.userRegister = new UserRegister('', this.f.emailAddress.value, '', this.f.password.value, this.firstName, this.lastName, this.f.phoneNumber.value, 0, '', true);
    this.accountService.registerObservable(this.userRegister).subscribe(api => {
      console.log(api);
      if (api.code === 200) {
        this.route.navigate(['/login']);
      }
      this.error = api.code;
    });
    // console.log(this.account.value);
  }
}
