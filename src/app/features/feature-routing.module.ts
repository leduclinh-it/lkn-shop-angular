import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import * as CONST from '../core/constants';
import { HomeComponent } from './components/home/home.component';
import { ProductListComponent } from './components/products/product-list/product-list.component';
import { ProductDetailComponent } from './components/products/product-detail/product-detail.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { DashboardComponent } from './components/admin/dashboard/dashboard.component';
import { UserListComponent } from './components/admin/user-list/user-list.component';
import { CartComponent } from './components/cart/cart.component';
import { CheckOutComponent } from './components/check-out/check-out.component';
import { AccountComponent } from './components/account/account.component';
import {AdminGuard} from '../shared/route-guard/admin.guard';
import {AdminLayoutComponent} from './main-layout/admin-layout/admin-layout.component';
import {UserLayoutComponent} from './main-layout/user-layout/user-layout.component';
import {ProductManageComponent} from './components/admin/product-manage/product-manage.component';
import {NewsComponent} from './components/news/news.component';
import {AboutComponent} from './components/about/about.component';
import {OrderNewComponent} from './components/admin/orders/order-new/order-new.component';
import {OrderCancelComponent} from './components/admin/orders/order-cancel/order-cancel.component';
import {OrderCompleteComponent} from './components/admin/orders/order-complete/order-complete.component';
import {OrderDetailComponent} from './components/admin/orders/order-detail/order-detail.component';
import {OrderUserComponent} from './components/order-user/order-user.component';
import {EmailConfirmComponent} from './components/email-confirm/email-confirm.component';
import {ConfirmResetComponent} from './components/confirm-reset/confirm-reset.component';
// @ts-ignore
const routes: Routes = [
  {path: '', redirectTo: CONST.FrontURL.HOME, pathMatch: 'full'},
  {
    path: '', component: UserLayoutComponent,
    children: [
      {path: CONST.FrontURL.CHECK_OUT, component: CheckOutComponent},
      {path: CONST.FrontURL.HOME, component: HomeComponent},
      {path: CONST.FrontURL.LOGIN, component: LoginComponent},
      {path: CONST.FrontURL.REGISTER, component: RegisterComponent},
      {path: CONST.FrontURL.PRODUCT_LIST + '/:status', component: ProductListComponent},
      {path: CONST.FrontURL.PRODUCT_DETAIL + '/:id', component: ProductDetailComponent},
      {path: CONST.FrontURL.NEWS, component: NewsComponent},
      {path: CONST.FrontURL.CART, component: CartComponent},
      {path: CONST.FrontURL.CHECK_OUT, component: CheckOutComponent},
      {path: CONST.FrontURL.ACCOUNT, component: AccountComponent, canActivate: [AdminGuard]},
      {path: CONST.FrontURL.ABOUT, component: AboutComponent},
      {path: CONST.FrontURL.ORDER_USER, component: OrderUserComponent, canActivate: [AdminGuard]},
      {path: CONST.FrontURL.EMAIL_CONFIRM, component: EmailConfirmComponent},
      {path: CONST.FrontURL.CONFIRM_PASSWORD, component: ConfirmResetComponent}
    ]
  },
  {
    path: 'admin', canActivate: [AdminGuard], component: AdminLayoutComponent,
    children: [
      {path: CONST.FrontURL.DASHBOARD, component: DashboardComponent},
      {path: CONST.FrontURL.USER_LIST, component: UserListComponent},
      {path: CONST.FrontURL.PRODUCT_MANAGE, component: ProductManageComponent},
      {path: CONST.FrontURL.ORDER_NEW, component: OrderNewComponent},
      {path: CONST.FrontURL.ORDER_CANCEL, component: OrderCancelComponent},
      {path: CONST.FrontURL.ORDER_COMPLETE, component: OrderCompleteComponent},
      {path: CONST.FrontURL.ORDER_DETAIL, component: OrderDetailComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FeatureRoutingModule { }
