import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FeatureRoutingModule } from './feature-routing.module';
import { HomeComponent } from './components/home/home.component';
import { ProductListComponent } from './components/products/product-list/product-list.component';
import { ProductDetailComponent } from './components/products/product-detail/product-detail.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HeaderComponent } from './main-layout/user-layout/header/header.component';
import { FooterComponent } from './main-layout/user-layout/footer/footer.component';
import {MDBBootstrapModule} from 'angular-bootstrap-md';
import { AdminLayoutComponent } from './main-layout/admin-layout/admin-layout.component';
import { UserLayoutComponent } from './main-layout/user-layout/user-layout.component';
import { HeaderAdminComponent } from './main-layout/admin-layout/header-admin/header-admin.component';
import { FooterAdminComponent } from './main-layout/admin-layout/footer-admin/footer-admin.component';
import { DashboardComponent } from './components/admin/dashboard/dashboard.component';
import { UserListComponent } from './components/admin/user-list/user-list.component';
import { CartComponent } from './components/cart/cart.component';
import { CheckOutComponent } from './components/check-out/check-out.component';
import { AccountComponent } from './components/account/account.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ProductManageComponent } from './components/admin/product-manage/product-manage.component';
import { ProductsComponent } from './components/products/product-list/products/products.component';
import { NewsComponent } from './components/news/news.component';
import { AboutComponent } from './components/about/about.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { OrderNewComponent } from './components/admin/orders/order-new/order-new.component';
import { OrderCompleteComponent } from './components/admin/orders/order-complete/order-complete.component';
import { OrderCancelComponent } from './components/admin/orders/order-cancel/order-cancel.component';
import { OrderDetailComponent } from './components/admin/orders/order-detail/order-detail.component';
import { ProductComponent } from './components/products/product-detail/product/product.component';
import { OrderUserComponent } from './components/order-user/order-user.component';
import { EmailConfirmComponent } from './components/email-confirm/email-confirm.component';
import { OrderUserDetailComponent } from './components/order-user/order-user-detail/order-user-detail.component';
import { OrderUserStatusComponent } from './components/order-user/order-user-status/order-user-status.component';
import { ConfirmResetComponent } from './components/confirm-reset/confirm-reset.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';


@NgModule({
  // tslint:disable-next-line:max-line-length
  declarations: [HomeComponent,
    ProductListComponent,
    ProductDetailComponent,
    LoginComponent,
    RegisterComponent,
    HeaderComponent,
    FooterComponent,
    AdminLayoutComponent,
    UserLayoutComponent,
    HeaderAdminComponent,
    FooterAdminComponent,
    DashboardComponent,
    UserListComponent,
    CartComponent,
    CheckOutComponent,
    AccountComponent,
    ProductManageComponent,
    ProductsComponent,
    NewsComponent,
    AboutComponent,
    OrderNewComponent,
    OrderCompleteComponent,
    OrderCancelComponent,
    OrderDetailComponent,
    ProductComponent,
    OrderUserComponent,
    EmailConfirmComponent,
    OrderUserDetailComponent,
    OrderUserStatusComponent,
    ConfirmResetComponent,
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    Ng2SearchPipeModule
  ],
  imports: [
    CommonModule,
    FeatureRoutingModule,
    MDBBootstrapModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule,
    Ng2SearchPipeModule
  ]
})
export class FeatureModule { }
