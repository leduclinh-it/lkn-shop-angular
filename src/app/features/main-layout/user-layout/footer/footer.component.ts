import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../../../core/services/authentication.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  constructor(private authenticationService: AuthenticationService) { }

  currentUser;
  ngOnInit(): void {
    this.currentUser = this.authenticationService.currentUserValue;
  }

}
