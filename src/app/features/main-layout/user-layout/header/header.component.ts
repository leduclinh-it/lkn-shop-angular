import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../../../core/services/authentication.service';
import {User} from '../../../../core/models/user';
import {Router} from '@angular/router';
import {CartService} from '../../../../core/services/cart.service';
import {AccountService} from '../../../../core/services/account.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  user: User;
  admin: User;
  count: number;
  constructor(private authenticationService: AuthenticationService,
              private route: Router,
              private cartService: CartService,
              private accountService: AccountService) {
  }

  ngOnInit(): void {
    this.cartService.currentQuantity.subscribe(x => {
      if (x) {
        this.count = x;
      } else {
        this.count = 0;
      }
    });
  }
  isUser() {
    const isUser = this.authenticationService.currentUserValue;
    if (isUser) {
      this.accountService.currentUser.subscribe(x => {
        if (x) {
          this.user = x;
        } else {
          this.user = isUser;
        }
      });
      return true;
    }
    return false;
  }
  isAdmin() {
    const isAdmin = this.authenticationService.isAdmin();
    if (isAdmin) {
      this.admin = isAdmin;
      return true;
    }
    return false;
  }

  logout() {
    this.authenticationService.logout();
    this.route.navigate(['/login']);
    this.cartService.getCart();
    this.cartService.updateQuantity(0);
  }

}
