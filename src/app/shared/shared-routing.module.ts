import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NotFoundComponent} from './components/error/not-found/not-found.component';
import {UserLayoutComponent} from '../features/main-layout/user-layout/user-layout.component';


const routes: Routes = [
  {path: '', component: UserLayoutComponent, children: [
      {path: '**', component: NotFoundComponent}
    ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SharedRoutingModule { }
