import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedRoutingModule } from './shared-routing.module';
import { NotFoundComponent } from './components/error/not-found/not-found.component';
import { SystemErrorComponent } from './components/error/system-error/system-error.component';


@NgModule({
  declarations: [NotFoundComponent, SystemErrorComponent],
  imports: [
    CommonModule,
    SharedRoutingModule,

  ]
})
export class SharedModule { }
