// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const serverHost = 'localhost:8080';
// https://lknshop.herokuapp.com
const clientHost = 'localhost:4200';
// const hostApiUrl = `http://${serverHost}`;
 const hostApiUrl = `https://lknshop.herokuapp.com`;

export const environment = {
  production: false,
  apiUrl: {
    account: {
      login: `${hostApiUrl}/api/auth/login`,
      logout: ``,
      register: `${hostApiUrl}/api/auth/register`
    },
    product: {
      products: `${hostApiUrl}/products/all`,
      detailProduct: `${hostApiUrl}/products/`,
      addProduct: `${hostApiUrl}/products/product`,
      editProduct: `${hostApiUrl}/products/product`,
      deleteProduct: `${hostApiUrl}/products/product/`,
      commentProduct: `${hostApiUrl}/comments/comment`,
      updateComment: `${hostApiUrl}/comments/comment/mergeComment`,
      statusProduct: `${hostApiUrl}/products/`,
      productPage: `${hostApiUrl}/products/page/`,
      productStatusPage: `${hostApiUrl}/products/status/page/`,
      productTotalPage: `${hostApiUrl}/products/status/num/`
    },
    user: {
      editProfile: `${hostApiUrl}/account/profile`,
      updatePassword: `${hostApiUrl}/account/update`,
      forgotPassword: `${hostApiUrl}/account/forgot-password/`,
      resetPassword: `${hostApiUrl}/account/confirm/`
    },
    cart: {
      cart: `${hostApiUrl}/cart`,
      changeCart: `${hostApiUrl}/cart/delete`,
      addCart: `${hostApiUrl}/cart/add`,
      checkout: `${hostApiUrl}/cart/checkout`,
    },
    order: {
      order: `${hostApiUrl}/orders/order`,
      orderUser: `${hostApiUrl}/orders/user`,
      orderById: `${hostApiUrl}/orders/`,
      orders: `${hostApiUrl}/orders/all`,
      cancel: `${hostApiUrl}/orders/order/cancel/`,
      finish: `${hostApiUrl}/orders/order/finish/`,
      findStatus: `${hostApiUrl}/orders/status/`,
      getPageStatus: `${hostApiUrl}/orders/status/user`,
      totalPageUser: `${hostApiUrl}/orders/status/user/num/`,
      totalPage: `${hostApiUrl}/orders/status/num/`,
      getPage: `${hostApiUrl}/orders/status/`,
    },
    file: {
      uploadFile: `${hostApiUrl}/uploadFile`,
      uploadMutiFile: `${hostApiUrl}/uploadMultipleFiles`,
    }
  },
  CONSTANT: {
    CURRENT_USER: 'CURRENT_USER',
    SECRET_KEY: 'LKNShop2019',
    CART: 'USER_CART'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
